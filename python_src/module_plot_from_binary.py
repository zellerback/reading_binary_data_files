#!/usr/bin/python

import numpy as np
import matplotlib.pyplot as plt
from module_read_binary_data import read_binary_data as read

def plot_from_binary(data_file_path, plot_title, plot_file_name):
    
    # 
    # ==> Read data from binary files
    #
    X, U, V = read(data_file_path)
    Y = X
    
    # Initialize plots
    fig0, ax0 = plt.subplots()
    
    # 
    # ==> Plot data
    #
    strm = ax0.streamplot(X, Y, U, V, color=U, linewidth=2, cmap=plt.cm.autumn)
    
    # Add color bar
    fig0.colorbar(strm.lines)
    
    # Set the plot title
    plt.title(plot_title) 
    
    # Save figure
    plt.savefig(plot_file_name)
    
    #
    # ==> Release memory
    #
    plt.close()
    
    
