program main

    use, intrinsic :: iso_fortran_env, only: &
        ip => INT32, &
        dp => REAL64

    ! Explicit typing only
    implicit none

    !--------------------------------------------------------------------------
    ! Dictionary
    !--------------------------------------------------------------------------
    real (dp),    pointer   :: x(:), u(:,:), v(:,:)
    real (dp),    parameter :: MESH = 6.0e-2_dp
    integer (ip), parameter :: N = 100
    integer (ip)            :: i, j, file_unit
    !--------------------------------------------------------------------------

    !
    !==> Allocate memory
    !
    allocate( x(N), u(N,N), v(N,N) )

    !
    !==> Compute grid x = [-3,3]
    !
    x = [ (-3.0_dp + real(i-1,kind=dp)*MESH, i=1, N) ]

    !
    !==> Compute velocities
    !
    do j = 1, N
        do i = 1, N
            u(i,j) = -1.0_dp - x(i)**2 + x(j)
            v(i,j) = 1.0_dp + x(i) - x(j)**2
        end do
    end do

    !
    !==> Write data to binary files
    !

    ! Create directory for data
    call execute_command_line( 'mkdir dat' )

    ! Write grid x
    open( newunit=file_unit, file='./dat/x.dat', &
        status='replace', form='unformatted', &
        action='write', access='stream')
    write( file_unit ) x
    close( file_unit )

    ! Write velocity u
    open( newunit=file_unit, file='./dat/u.dat', &
        status='replace', form='unformatted', &
        action='write', access='stream')
    write( file_unit ) u
    close( file_unit )

    ! Write velocity v
    open( newunit=file_unit, file='./dat/v.dat', &
        status='replace', form='unformatted', &
        action='write', access='stream')
    write( file_unit ) v
    close( file_unit )

    !
    !==> Release memory
    !
    deallocate( x, u, v )

end program main
