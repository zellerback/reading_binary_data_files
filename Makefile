include make.inc

all: fortran cpp python

fortran:
	( cd ./fortran_src; $(MAKE) clean; $(MAKE) all)

cpp:
	( cd ./cpp_src; $(MAKE) clean; $(MAKE) all)

python:
	( cd ./python_src; python main.py )

clean: 
	( cd ./fortran_src; $(MAKE) clean; cd ../cpp_src; $(MAKE) clean )

.PHONY: all fortran cpp python