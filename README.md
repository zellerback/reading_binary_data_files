# **read\_binary\_data\_files**

This project demonstrates how to use Python for post-processing binary files written by Fortran and C/C++.

-----------------------------------------------------------------------------

## Requirements
* The GNU Make tool <https://www.gnu.org/software/make/>
* The GNU gfortran compiler <https://gcc.gnu.org/wiki/gFortran>
* The GNU g++ compiler <https://gcc.gnu.org/>
* Python 2.7.10 (or newer) <https://www.python.org/downloads/release>
* NumPy 1.10.2, SciPy 0.16.1 (or newer) <http://www.scipy.org/scipylib/download.html>

-----------------------------------------------------------------------------

## To build the project

Type the following command line arguments
```
git clone https://zellerback@bitbucket.org/zellerback/reading_binary_data_files.git

cd reading_binary_data_files; make all
```